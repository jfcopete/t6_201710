package test;

import junit.framework.TestCase;
import model.data_structures.EncadenamientoSeparado;
public class PruebaEncadenamientoSeparado extends TestCase{

	private EncadenamientoSeparado<String, Integer> tabla;
	
	
	public void setup1(){
		tabla = new EncadenamientoSeparado<String, Integer>(10);
	}
	
	public void setup2(){
		tabla = new EncadenamientoSeparado<String, Integer>(10);
		tabla.insertar("Carro",0);
		tabla.insertar("�rbol",1);
		tabla.insertar("Rat�n", 58);
		tabla.insertar("Ashwiujsen",20);
		tabla.insertar("sdfawoiiiieur",001251);
		tabla.insertar("QpiuEAKSMVEnus2", 54847);
		tabla.insertar("sdjkuqyuyyyyyeajMichael", 25455);
		tabla.insertar("SaludosPerros",123456789);
		tabla.insertar("DejeDePerderElTiempo",5844);
		tabla.insertar("sdjkuqyuyyyyyeajMichael", 5);
	}
	
	public void setup3(){
		tabla = new EncadenamientoSeparado<String, Integer>(10);
		tabla.insertar("Carro",0);
		tabla.insertar("�rbol",1);
		tabla.insertar("Rat�n", 58);
		tabla.insertar("Ashwiujsen",20);
		tabla.insertar("sdfawoiiiieur ",001251);
		tabla.insertar("QpiuEAKSMVEnus2", 54847);
		tabla.insertar("sdjkuqyuyyyyyeajMichael", 25455);
		tabla.insertar("SaludosPerros",123456789);
		tabla.insertar("DejeDePerderElTiempo",5844);
		tabla.insertar("sdjkuqyuyyyyyeajMichael", 5);
		tabla.insertar("", 20);
		tabla.insertar("Estofado", 31);
		tabla.insertar("45", 878778);
		tabla.insertar("EstoEst�ComoLargo", 8);
		tabla.insertar("Nada que termino", 987546018);
		tabla.insertar("Porqu�Puse20", 2187);
		tabla.insertar("RetiradoPorWebPrroz",485);
		tabla.insertar("SilviaNoexplicanimierda", 579993448);
		tabla.insertar("Poresoretirelenguajes",784);
		tabla.insertar("Mejorlaveoconcardozo", 5488);
		
	}
	
	
	public void darTamanio(){
		setup1();
		assertEquals("El tama�o de la tabla deber�a ser 0", 0, tabla.darTamanio());
		setup2();
		assertEquals("El tama�o de la tabla deber�a ser 9",9 , tabla.darTamanio());
		setup3();
		assertEquals("El tama�o de la tabla deber�a ser 19", 19, tabla.darTamanio());
	}
	
	
	
	public void estaVaciaTest(){
		setup1();
		assertEquals("La tabla deber�a estar vac�a",true,tabla.estaVacia());
		setup2();
		assertEquals("La tabla no deber�a estar vac�a",false, tabla.estaVacia());
		setup3();
		assertEquals("La tabla no deber�a estar vac�a",false, tabla.estaVacia());

	}
	
	
	public void darValorTest(){
		setup1();
		assertNull("El valor deber�a ser null", tabla.darValor("Carro"));
		setup2();
		assertSame("El valor esperado para la llave 'Carro' es: 0",0,tabla.darValor("Carro"));
		setup3();
		assertSame("El valor esperado para la llave 'sdjkuqyuyyyyyeajMichael' es: 5",5,tabla.darValor("sdjkuqyuyyyyyeajMichael"));
	}
	
	
	public void insertarTest(){
		setup1();
		assertEquals("La tabla deber�a estar vac�a", true, tabla.estaVacia());
		tabla.insertar("Ana Maria", 13);
		
		assertEquals("La tabla no deber�a estar vac�a", false, tabla.estaVacia());
		assertSame("El valor esperado para la llave 'Ana Maria' es: 13",13,tabla.darValor("Ana Maria"));
		setup3();
		
		assertEquals("La tabla no deber�a estar vac�a", false, tabla.estaVacia());
		tabla.insertar("Ana Maria", 13);
		assertEquals("El tama�o de la tabla deber�a ser 20",20,tabla.darTamanio());
	}
	
	
	public void darLongitudListasTest(){
		
		setup1();
		int[] tamanioListas = tabla.darLongitudListas();
		int sizeSlot;
		int numSlot;
		for(int i =0; i< tamanioListas.length; i++){
			sizeSlot = tamanioListas[i];
			numSlot =i;
			 if(numSlot == 0){assertEquals("El tama�o de la lista en el slot 0 deber�a ser de 0",0,sizeSlot);}
			 else if(numSlot == 1){assertEquals("El tama�o de la lista en el slot 1 deber�a ser de 0",0, sizeSlot);}
			 else if(numSlot == 2){assertEquals("El tama�o de la lista en el slot 2 deber�a ser de 0",0, sizeSlot);}
			 else if(numSlot == 3){assertEquals("El tama�o de la lista en el slot 3 deber�a ser de 0",0, sizeSlot);}
			 else if(numSlot == 4){assertEquals("El tama�o de la lista en el slot 4 deber�a ser de 0",0, sizeSlot);}
			 else if(numSlot == 5){assertEquals("El tama�o de la lista en el slot 5 deber�a ser de 0",0, sizeSlot);}
			 else if(numSlot == 6){assertEquals("El tama�o de la lista en el slot 6 deber�a ser de 0",0, sizeSlot);}
			 else if(numSlot == 7){assertEquals("El tama�o de la lista en el slot 7 deber�a ser de 0",0, sizeSlot);}
			 else if(numSlot == 8){assertEquals("El tama�o de la lista en el slot 8 deber�a ser de 0",0, sizeSlot);}
			 else if(numSlot == 9){assertEquals("El tama�o de la lista en el slot 9 deber�a ser de 0",0, sizeSlot);}
			
		}
		setup2();
		tamanioListas = tabla.darLongitudListas();
		for(int i =0; i< tamanioListas.length; i++){
			sizeSlot = tamanioListas[i];
			numSlot =i;
			 if(numSlot == 0){assertEquals("El tama�o de la lista en el slot 0 deber�a ser de 0",0,sizeSlot);}
			 else if(numSlot == 1){assertEquals("El tama�o de la lista en el slot 1 deber�a ser de 0",0, sizeSlot);}
			 else if(numSlot == 2){assertEquals("El tama�o de la lista en el slot 2 deber�a ser de 1",1, sizeSlot);}
			 else if(numSlot == 3){assertEquals("El tama�o de la lista en el slot 3 deber�a ser de 1",1, sizeSlot);}
			 else if(numSlot == 4){assertEquals("El tama�o de la lista en el slot 4 deber�a ser de 4",4, sizeSlot);}
			 else if(numSlot == 5){assertEquals("El tama�o de la lista en el slot 5 deber�a ser de 0",0, sizeSlot);}
			 else if(numSlot == 6){assertEquals("El tama�o de la lista en el slot 6 deber�a ser de 0",0, sizeSlot);}
			 else if(numSlot == 7){assertEquals("El tama�o de la lista en el slot 7 deber�a ser de 2",2, sizeSlot);}
			 else if(numSlot == 8){assertEquals("El tama�o de la lista en el slot 8 deber�a ser de 1",1, sizeSlot);}
			 else if(numSlot == 9){assertEquals("El tama�o de la lista en el slot 9 deber�a ser de 0",0, sizeSlot);}
			
		}

	}
	
	public void tieneLlaveTest(){
		setup1();
		assertEquals("No deber�a tener la llave: 'Carro'",false, tabla.contiene("Carro"));
		setup2();
		assertEquals("Deber�a contener la llave: 'Carro'",true, tabla.contiene("Carro"));
	}

}
