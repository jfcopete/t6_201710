package model;

import model.data_structures.EncadenamientoSeparado;
import model.data_structures.ListaLlaveValorSecuencial;
import model.data_structures.Node;
import histogram.*;

public class Experimento<K extends Comparable<K>, V extends Comparable<V>> {
	private EncadenamientoSeparado<String,Integer> encadenamientoExperimento;
	private ListaLlaveValorSecuencial<String, Integer> listaAux; 
	private GeneradorDatos a = new GeneradorDatos(); 

	public String[] cadenas(int n, int k){
		String[] b = a.generarCadena(n, k); 
		return b; 
	}

	public Integer[] numeros(int N){
		Integer[] c = a.generarNumeros(N); 
		return c; 
	}
	public void generar(int n, int k){
		String[] d = cadenas(n, k); 
		Integer[] e = numeros(n);  
		encadenamientoExperimento = new EncadenamientoSeparado<>(n); 
		for (int i = 0 ; i < d.length ; i++){
			String llave = d[i];
			int valor = e[i]; 
			encadenamientoExperimento.insertar(llave, valor);	
		}

	}
	public void prueba(int n, int k){

		Integer[] e = numeros(n);  
		String[] d = cadenas(n, k); 
		encadenamientoExperimento = new EncadenamientoSeparado<>(n); 

		for (int i = 0 ; i < e.length ; i++){
			String llave = d[i];
			int valor = e[i]; 
			encadenamientoExperimento.insertar(llave, valor);

		}
		Histogram x = new Histogram(encadenamientoExperimento.darTamanio()); 
		int[] ac = encadenamientoExperimento.darLongitudListas(); 
		for(int i = 0; i < ac.length ; i++){
			int a = ac[i]; 
			System.out.println(a);
		}
		
		
//		for (int i = 0 ; i < ac.length ; i++){
//			int max = 0; 
//			if (ac[i]>max){max = ac[i];}
//			x.addDataPoint(ac[i]);	
//		}
//		x.draw();
	}

}
