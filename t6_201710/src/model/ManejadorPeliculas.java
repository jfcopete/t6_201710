package model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.Gson;

import model.data_structures.EncadenamientoSeparado;

public class ManejadorPeliculas {
	private EncadenamientoSeparado<String,VOInfoPelicula> tabla; 
 
	public void cargarPeliculas(){ 
		Gson g = new Gson(); 
		try {
			BufferedReader br = new BufferedReader(new FileReader("links_json.json"));
			
			VOInfoPelicula[] x = g.fromJson(br, VOInfoPelicula[].class) ;
			tabla = new EncadenamientoSeparado<>(x.length); 
			for (int i = 0 ; i < x.length ; i++){
				tabla.insertar(x[i].getMovieId(), x[i]);
			}
			int[] test = tabla.darLongitudListas();
			for (int a = 0; a < test.length ; a++){
				System.out.println(test[a]);
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
