package model;
import com.google.gson.annotations.Expose;

import com.google.gson.annotations.SerializedName;


public class ImdbData {

	@SerializedName("Plot")
	@Expose
	private String plot;
	@SerializedName("Rated")
	@Expose
	private String rated;
	@SerializedName("Response")
	@Expose
	private String response;
	@SerializedName("Language")
	@Expose
	private String language;
	@SerializedName("Title")
	@Expose
	private String title;
	@SerializedName("Country")
	@Expose
	private String country;
	@SerializedName("Writer")
	@Expose
	private String writer;
	@SerializedName("Metascore")
	@Expose
	private String metascore;
	@SerializedName("imdbRating")
	@Expose
	private String imdbRating;
	@SerializedName("Director")
	@Expose
	private String director;
	@SerializedName("Released")
	@Expose
	private String released;
	@SerializedName("Actors")
	@Expose
	private String actors;
	@SerializedName("Year")
	@Expose
	private String year;
	@SerializedName("Genre")
	@Expose
	private String genre;
	@SerializedName("Awards")
	@Expose
	private String awards;
	@SerializedName("Runtime")
	@Expose
	private String runtime;
	@SerializedName("Type")
	@Expose
	private String type;
	@SerializedName("Poster")
	@Expose
	private String poster;
	@SerializedName("imdbVotes")
	@Expose
	private String imdbVotes;
	@SerializedName("imdbID")
	@Expose
	private String imdbID;

	public String getPlot() {
		return plot;
	}

	public void setPlot(String plot) {
		this.plot = plot;
	}

	public String getRated() {
		return rated;
	}

	public void setRated(String rated) {
		this.rated = rated;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public String getMetascore() {
		return metascore;
	}

	public void setMetascore(String metascore) {
		this.metascore = metascore;
	}

	public String getImdbRating() {
		return imdbRating;
	}

	public void setImdbRating(String imdbRating) {
		this.imdbRating = imdbRating;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getReleased() {
		return released;
	}

	public void setReleased(String released) {
		this.released = released;
	}

	public String getActors() {
		return actors;
	}

	public void setActors(String actors) {
		this.actors = actors;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getAwards() {
		return awards;
	}

	public void setAwards(String awards) {
		this.awards = awards;
	}

	public String getRuntime() {
		return runtime;
	}

	public void setRuntime(String runtime) {
		this.runtime = runtime;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPoster() {
		return poster;
	}

	public void setPoster(String poster) {
		this.poster = poster;
	}

	public String getImdbVotes() {
		return imdbVotes;
	}

	public void setImdbVotes(String imdbVotes) {
		this.imdbVotes = imdbVotes;
	}

	public String getImdbID() {
		return imdbID;
	}
}

