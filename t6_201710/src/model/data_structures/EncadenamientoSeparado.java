package model.data_structures;

import java.util.Iterator;

public class EncadenamientoSeparado <K extends Comparable<K>,V>implements IEncadenamientoSeparado<K, V>  {

	private int N;
	private int M;
	private ListaLlaveValorSecuencial<K, V> [] st;

	public EncadenamientoSeparado(int M) {
		this.M = M;
		st = (ListaLlaveValorSecuencial<K, V>[]) new ListaLlaveValorSecuencial[M];
		for (int i = 0; i < M; i++) {
			st[i]= new ListaLlaveValorSecuencial<>();
		}
	}
	
	
	@Override
	public Iterator<K> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int darTamanio() {
		return st.length;
	}

	@Override
	public boolean estaVacia() {
		return st.length==0;
	}

	@Override
	public V darValor(K llave) {
		return (V)st[hash(llave)].darValor(llave);
	}

	@Override
	public void insertar(K llave, V valor) {
		st[hash(llave)].insertar(llave, valor);
		
	}

	@Override
	public Iterable<K> llaves() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int hash(K llave) {
		return (llave.hashCode()&0x7fffffff%M);
	}

	@Override
	public int[] darLongitudListas() {
			int[] longitud= new int[M];
			for (int i = 0; i < st.length; i++) {

				ListaLlaveValorSecuencial<K, V> lista= new ListaLlaveValorSecuencial<>();
				lista= st[i];

				longitud[i]= lista.darTamanio();
			}
			return longitud;
	}
	
	public boolean contiene(K llave) {
		return darValor(llave)!=null;
	}
	
	public void delete(K llave)
	{
		insertar(llave, null);
	}
}