package model.data_structures;

public interface IListaLlaveValorSecuencial<K,V> {
public void ListaLlaveValorSecuencial(); 
public int darTamanio(); 
public boolean estaVacia(); 
public boolean tieneLlave(K llave);
public V darValor(K llave);
public void insertar(K llave, V valor); 
public Iterable<K> llaves(); 


}
