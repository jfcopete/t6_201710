package model.data_structures;

public class Node<K,V> 
{
	private K llave;
	private V valor;

	private Node<K,V> next;

	public Node(K pllave, V pvalor)
	{
		next = null;
		this.llave= pllave; 
		this.valor=pvalor; 
	}


	public Node() {
		// TODO Auto-generated constructor stub
	}

	public K getLlave() {
		return llave;
	}
	public V getValor() {
		return valor;
	}

	public void setLlave(K llave) {
		this.llave = llave;
	}
	public void setValor(V valor) {
		this.valor = valor;
	}

	public Node<K,V> getNext() 
	{
		return next;
	}


	public void setNext(Node<K,V> next) 
	{
		this.next = next;
	}
	public boolean hasNext(){
		if (this.next != null) return true; 
		else return false; 
	}


	

	
	
}
