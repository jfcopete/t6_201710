package model.data_structures;

public interface IEncadenamientoSeparado<K,V> extends Iterable<K>{
	
	
	/**
	 * Devuelve el n�mero de llaves en la tabla
	 * @return
	 */
	public int darTamanio();
	
	/**
	 * Devuelve si la tabla est� vac�a o no
	 * @return
	 */
	public boolean estaVacia();
	
	/**
	 * Devuelve si la llave existe en la tabla
	 * @param llave
	 * @return
	 */
	public V darValor(K llave);
		
	/**
	 * Inserta la llave y el valor en la tabla. Si el valor es
	 * null y la llave existe previamente debe eliminar el
	 * elemento de la tabla. Si la llave existe debe
	 * reemplazar el valor asociado a la llave. Recuerde
	 * reducir las posiciones de la tabla o aumentarlas
	 * (rehash) dependiendo del factor de carga de la
	 * tabla. (n/m) 
	 * @param llave
	 * @param valor
	 */
	public void insertar(K llave, V valor);
	
	/**
	 * Devuelve un iterador sobre las llaves de la tabla
	 * @return
	 */
	public Iterable<K> llaves();
	
	/**
	 * Devuelve la posici�n de 0 a m-1 indicada para
	 * guardar la llave en la tabla
	 * @param llave
	 * @return
	 */
	int hash(K llave);
	
	/**
	 * Devuelve la longitud de cada una de las listas que
	 * pertenecen a la tabla de hash.
	 * @return
	 */
	public int [] darLongitudListas();
	
}
