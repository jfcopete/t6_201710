package model;
import com.google.gson.annotations.Expose;

import com.google.gson.annotations.SerializedName;


public class VOInfoPelicula  {

@SerializedName("movieId")
@Expose
private String movieId;
@SerializedName("imdbId")
@Expose
private String imdbId;
@SerializedName("tmdbId")
@Expose
private String tmdbId;
@SerializedName("imdbData")
@Expose
private ImdbData imdbData;

public String getMovieId() {
return movieId;
}

public void setMovieId(String movieId) {
this.movieId = movieId;
}

public String getImdbId() {
return imdbId;
}

public void setImdbId(String imdbId) {
this.imdbId = imdbId;
}

public String getTmdbId() {
return tmdbId;
}

public void setTmdbId(String tmdbId) {
this.tmdbId = tmdbId;
}

public ImdbData getImdbData() {
return imdbData;
}

public void setImdbData(ImdbData imdbData) {
this.imdbData = imdbData;
}


}