package model;

import java.util.Random;



public class GeneradorDatos {
Random rdn = new Random(); 

	public String getCadenaAlfanumAleatoria (int longitud){
		String cadenaAleatoria = "";
		long milis = new java.util.GregorianCalendar().getTimeInMillis();
		Random r = new Random(milis);
		int i = 0;
		while ( i < longitud){
			char c = (char)r.nextInt(255);
			if ( c >='b' && c <='z' ){
				cadenaAleatoria += c;
				i ++;
			}
		}
		return cadenaAleatoria;
	}

	public String[] generarCadena(int n, int k){ 
		String[] vector = new String[n]; 
		for (int i = 0 ; i<n; i++){
			 
			int longitudCadena = /**(int) (rdn.nextDouble() * 15 +2)*/ 7; 
			String cadena = getCadenaAlfanumAleatoria(longitudCadena); 
			String as = ""; 
			for (int j = 0 ; j < k ; j++){
				as += "a"; 
			}
			vector[i] = as +cadena ; 
		}
		return vector; 
	}
	
	public Integer[] generarNumeros(int n){
		Integer[] numero = new Integer[n]; 
		for(int i = 0 ; i < n ; i++){
			int num = (int) (rdn.nextDouble() * 2000); 
			numero[i] = num; 
		}
		return numero; 
	}
}
