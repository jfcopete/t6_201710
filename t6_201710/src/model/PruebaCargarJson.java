package model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;

import com.google.gson.Gson;


public class PruebaCargarJson<T> {

	public void cargar(){
		Gson g = new Gson();
		BufferedReader br = null; 
		try {
			br = new BufferedReader(new FileReader("links_json.json"));
			VOInfoPelicula[] resultado = g.fromJson(br, VOInfoPelicula[].class); 
			for(VOInfoPelicula t : resultado){
				System.out.println("ID de la Pelicula:" + " " + t.getMovieId() +
						  "\n" + "ID de IMBd: " + t.getImdbId() + "\n" + "Resumen : " +  t.getImdbData().getPlot() + "\n" + "Calificacion: " + t.getImdbData().getRated()+ "\n" + "Reaccion: " + t.getImdbData().getResponse()+"\n" +"Lemguaje: "+ t.getImdbData().getLanguage() +"\n"+"Titulo: " + t.getImdbData().getTitle()+ "\n" +"Pais: "+ t.getImdbData().getCountry() +"\n"+ "Escritor: "+t.getImdbData().getWriter() +"\n"+ "Metascore: " +t.getImdbData().getMetascore()+"\n" +"Rating de imbd: " +  t.getImdbData().getImdbRating() +"\n"+ "Director :  "+  t.getImdbData().getDirector() +"\n"+ "Fecha de lanzamiento: "+ t.getImdbData().getReleased() +"\n"+ "Actores: "+t.getImdbData().getActors()+"\n" +"Agnio: " +t.getImdbData().getYear() +"\n"+ "Genero : "+ t.getImdbData().getGenre() +"\n"+ "Premios: "+ t.getImdbData().getAwards() +"\n"+ "Runtime: "+t.getImdbData().getRuntime() +"\n"+"Tipo: "+ t.getImdbData().getType() +"\n"+"Poster: " +t.getImdbData().getPoster() +"\n"+ "Votos de Imbd: "+t.getImdbData().getImdbVotes() +"\n"+"ID de Imbd: "+ t.getImdbData().getImdbID());
			}
			System.out.println(resultado.length);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}




}
