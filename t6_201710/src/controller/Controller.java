package controller;
import model.Experimento;
import model.GeneradorDatos;
import model.ManejadorPeliculas;
import model.PruebaCargarJson;
import model.VOInfoPelicula;
import model.data_structures.ListaEncadenada;
public class Controller {

	private static GeneradorDatos g = new GeneradorDatos(); 
	private static PruebaCargarJson pcg = new PruebaCargarJson(); 
	private static Experimento e = new Experimento<>(); 
	private static ManejadorPeliculas f = new ManejadorPeliculas(); 
	
	public static String[] generarCadenas(int k,int n){
		return g.generarCadena(n, k); 
	}
	
	public static Integer[] generarNumeros(int n){
		return g.generarNumeros(n); 
	}
	
	public static void cargar(){
		pcg.cargar();
	}
	public static void experimento(int n, int k){
		e.generar(n, k);
	}
	public static void histograma(int n, int k){
		e.prueba(n,k);
	}
	public static void tablaJson(){
		f.cargarPeliculas();
	}
}
