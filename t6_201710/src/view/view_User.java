package view;

import java.util.Scanner;

import controller.Controller;
import model.VOInfoPelicula;
import model.data_structures.ListaEncadenada;

public class view_User {

	private static long timeStampInicial; 
	private static long timeStampFinal; 
	
	public static void printMenu(){
		System.out.println("1. generar cadenas");
		System.out.println("2. generar numeros");
		System.out.println("3. prueba cargar Json");
		System.out.println("4. para experimentar con datos aleatorios");
		System.out.println("5. Histograma");
		System.out.println("6. generar tabla de hash del Json e imprimir el tiempo de carga");
	}
	
	public static void main(String [] args){
		boolean fin = false; 
		while (fin != true){
			printMenu();
			Scanner sc = new Scanner(System.in); 
			int option = sc.nextInt(); 
			switch(option){
			case 1 : 
				System.out.println("¿cuantas cadenas desea generar?");
				Scanner cadenas = new Scanner(System.in); 
				int n = cadenas.nextInt();  
				System.out.println("¿cuantas veces desea que se repita el caracter 'a' ?");
				Scanner kk = new Scanner(System.in); 
				int k = kk.nextInt(); 
				String[] data = Controller.generarCadenas(k,n); 
				for(int i = 0 ; i< data.length; i++){
					System.out.println(data[i].toString());
				}
				break; 
			case 2 : 
				System.out.println("¿cuantos numero desea generar?");
				Scanner numeros = new Scanner(System.in); 
				int n2 = numeros.nextInt(); 
				Integer[] data2 = Controller.generarNumeros(n2); 
				for (int i = 0 ; i< data2.length ; i++){
					System.out.println("" + data2[i].toString());
				}
				break;
			case 3: 
				Controller.cargar(); 
				break; 
			case 4: 
				System.out.println("¿cuantos datos desea generar?");
				Scanner num = new Scanner(System.in); 
				int n3 = num.nextInt(); 
				System.out.println("¿cuantas veces desea que se repita el caracter a");
				Scanner chara = new Scanner(System.in); 
				int k3 = chara.nextInt(); 
				long inicio = System.currentTimeMillis(); 
				Controller.experimento(n3, k3);
				long end = System.currentTimeMillis(); 
				System.out.println("el tiempo fue de : " + (end-inicio) + "milisegundos");
				break;
			case 5: 
				System.out.println("¿cuantos datos desea generar?");
				Scanner num2 = new Scanner(System.in); 
				int n4 = num2.nextInt(); 
				System.out.println("¿cuantas veces desea que se repita el caracter a");
				Scanner chara2 = new Scanner(System.in); 
				int k4 = chara2.nextInt(); 
				Controller.histograma(n4,k4);
				break; 
			case 6 : 
				long begin = System.currentTimeMillis(); 
				Controller.tablaJson();
				long stop = System.currentTimeMillis(); 
				System.out.println("el tiempo fue de: " + (stop-begin) + "milisegundos");
				default : 
					fin = true; 
					break; 
			}
		}
	}
}
